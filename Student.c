#include <sys/shm.h>
#include <errno.h>
#include <sys/sem.h>
#include <math.h>
#include "utility.h"
#define randomN(min, max) (rand() %(max-min+1))+min
#define randomize srand((unsigned)time(NULL)) 

enum state {searching, inviting, group_closed} student_state;  
boolean EXIT = false, wait_for_swap, exit_sem;
group my_group;
student myself;
control_data control;
int index_student, index_group; // indici per ottenere i valori nella shared memory senza iterare
int POP_SIZE, SIM_TIME, ACTUAL_TIME;
int *alreadySent;
student* data;
group* groups;
int* phase;

void handle_shut_down(int sig);
void handle_change_state(int sig);
void handle_end(int sig);
void enterGroup(int leader_matr);
void autoRefuse();
boolean checkAvail(int matr);
boolean checkAccept(int grade, int pref);
int checkEntranceMessages();
int dequeueRejectionMessages();
boolean checkMatch(int);
void closeGroup();

int main(int argc, char *argv[]){
	randomize;
	int check, error, change_state, sem_reserved, st, num_msg_sent, 
	num_response_received, i, index;
	char* saluto;
	wait_for_swap = true; //Booleano per non far cambiare subito stato a chi riceve. DIventa true dopo segnale di allarme
	i = 0;
	index = 0;
	num_msg_sent = 0;
	num_response_received = 0;
	st = 0;
	struct msqid_ds data_queue;
	boolean check_free = false, change = false, group_created = false, go_invite = true;
	
	struct msgg m, res, invite;

	//Spacchettamento dei dati dall Handler
	control.set_semaphore = atoi(argv[1]);
	int nof_elem = atoi(argv[2]);
	control.mem_groups = atoi(argv[3]);
	int nof_invites = atoi(argv[4]);
	control.mem_data = atoi(argv[5]);
	int max_rejects = atoi(argv[6]);
	SIM_TIME = atoi(argv[7]);
	POP_SIZE = atoi(argv[8]);
	control.mem_time = atoi(argv[9]);
	control.group_set = atoi(argv[10]);

	//Variabile per salvare il nuovo tempo
	ACTUAL_TIME = SIM_TIME;

	//Array per persone che ho già invitato
	alreadySent = malloc(sizeof(int)*POP_SIZE);
	for(int i = 0; i<POP_SIZE; i++){
		*(alreadySent+i) = 0;
	}
	
	//Attach delle memorie condivise
	groups = (group*)shmat(control.mem_groups, NULL, 0);
	data = (student*)shmat(control.mem_data, NULL, 0);
	phase = (int*)shmat(control.mem_time, NULL, 0);

	//Riprende i miei dati da shared_memory, essendo solo lettura non richiede blocco del semaforo
	for(int s = 0; s < POP_SIZE;s++){
		if((*(data+s)).matr == getpid()){
			myself = *(data+s);
			index_student = s;
		}
	}
	
	//Creazione del proprio gruppo e salvataggio nella variabile locale my_group
	student_state = searching;
	my_group.leader = myself.matr;
	my_group.preference = myself.preference;
	my_group.num_members = 1;
	my_group.members[0] = myself;
	my_group.grade = myself.arch_grade;
	my_group.closed = false;

	//Salvataggio del proprio gruppo nella memoria condivisa
	if(reserveSem(control.set_semaphore,3)<0){
		perror("RES_GROUP'S SETTING");
		kill(getppid(),SIGTERM);
	}
	for(int i = 0; i< POP_SIZE && !group_created;i++){
		if((*(groups+i)).leader == 0){
			(*(groups+i)) = my_group;
			group_created = true;
			index_group = i;
		}
	}
	if(releaseSem(control.set_semaphore,3)<0){
		perror("REL_GROUP'S SETTING");
		kill(getppid(),SIGTERM);
	}

	//Impostazione gestore per segnali di terminazione
	if(signal(SIGTERM,handle_shut_down)<0){
		perror("Error sigaction");
		kill(getppid(),SIGTERM);
	}

	//Impostazione gestore per segnali di tipo SIGUSR1
	if(signal(SIGUSR1,handle_end)<0){
		perror("Error sigaction");
		kill(getppid(),SIGTERM);
	}

	//Impostazione gestore per segnali di allarme
	if(signal(SIGALRM,handle_change_state)<0){
		perror("Error sigaction");
		kill(getppid(),SIGTERM);
	}
	
	//Ottenimento la coda di messaggi che ha come id il pid del padre
	control.start_queue = msgget(getppid(),0660|IPC_EXCL); 
	if(control.start_queue <0){
		printf("msggetError\n");
		kill(getppid(),SIGTERM);
	}
	
	//Avviso il padre che lo Studente ha finito di prepararsi
	if(releaseSem(control.set_semaphore,1)<0){
		perror("REL_END");
		kill(getppid(),SIGTERM);
	}
	
	//Attesa della concessione del padre per partire
	if(reserveSem(control.set_semaphore,0)<0){
		perror("RES_GO_SEM");
		kill(getppid(),SIGTERM);
	}

	//Inizio simulazione
	while(!EXIT){
		//Controllo della memoria condivisa del tempo per cambiare comportamento
		switch((*phase)){
			case 1:
				ACTUAL_TIME = 3*((int)SIM_TIME/4);
				break;
			case 2:
				ACTUAL_TIME = (int)SIM_TIME/2;
				break;
			case 3:
				ACTUAL_TIME = (int)SIM_TIME/4;
				break;
			case 4:
				ACTUAL_TIME = (int)SIM_TIME/4;
				break;
		}
		switch(student_state){
			//Attesa di un messaggio
			case 0:  
				check = msgrcv(control.start_queue,&invite,sizeof(invite) - sizeof(long), myself.matr, IPC_NOWAIT); 
				if(check < 0){
					error = errno;
					if(error == ENOMSG ){ //Se non ci sono messaggi prova a cambiare inviti
						if(nof_invites){ //Se non hai più inviti non puoi cambiare
							//Scelta se cambiare stato o no, basata sul tempo e sul voto
						 	if((myself.arch_grade > 24 || ACTUAL_TIME == 3*((int)SIM_TIME/4) && myself.arch_grade > 20 || (ACTUAL_TIME == (int)SIM_TIME/2 || ACTUAL_TIME == (int)SIM_TIME/4) ) && wait_for_swap){ //CHOOSE IF IT WANT TO CHANGE
								student_state = inviting;
								go_invite = true;
							}
						}
					}
					else{ //Se l'errore non è ENOMSG fai terminare al padre la simulazione
						perror("MSGRCV 1");
						printf("ERRNO: %d",error);
						kill(getppid(),SIGTERM);
					}
				}
				else{
					//spacchettamento messaggio
					int inv_ind;
					int inv_mat;
					inv_ind = atoi(invite.mtext); //Indice nella memoria condivisa dei gruppi dello Studente che ha mandato l'invito					
					boolean weight = false; //Variabile per la decisione di entrare nel gruppo o meno
					boolean e = true;
					sem_reserved = 0; //Variabile per gestire il caso in cui esco dalla reserve senza riservare il semaforo
					//Prenotazione semaforo per modificare la memoria condivisa dei gruppi riguardante il gruppo dello Studente che mi ha invitato
					exit_sem = false;				
					while(!EXIT && !exit_sem){
						if(reserveSem(control.group_set,inv_ind)<0){
							error = errno;
							if(error != EINTR){
								perror("RES_STATE0_GROUP");
								kill(getppid(),SIGTERM);
							}
						}
						else {
							exit_sem = true;
							sem_reserved++;
						}
					}

					//Prenotazione semaforo per modificare la memoria condivisa dei gruppi riguardante il mio gruppo
					exit_sem = false;
					while(!EXIT && !exit_sem){
						if(reserveSem(control.group_set,index_group)<0){
							error = errno;
							if(error != EINTR){
								perror("RES_STATE0_GROUP");
								kill(getppid(),SIGTERM);
							}
						}
						else {
							exit_sem = true;
							sem_reserved++;
						}
					}

					inv_mat =(*(groups+inv_ind)).leader; //Salvo la matricola della persona che mi ha invitato
					if((*(groups+inv_ind)).num_members < (*(groups+inv_ind)).preference &&!EXIT){//Se c'è posto o se non è finita la simulazione
						if(checkAccept((*(groups+inv_ind)).grade,(*(groups+inv_ind)).preference)|| max_rejects==0){
							weight = true;
							//Modifica del gruppo del proprietario e aggiornamento del mio gruppo
							int num = (*(groups+inv_ind)).num_members;
							(*(groups+inv_ind)).members[num] = myself;
							(*(groups+inv_ind)).num_members++;
							(*(groups+inv_ind)).grade = max(myself.arch_grade, (*(groups+inv_ind)).grade);
							my_group = (*(groups+inv_ind));
							(*(groups+index_group)).leader = 0; //Annullo il mio gruppo nella memoria condivisa
						}
						else{
							max_rejects--;
						}
					}

					//rilascio semaforo della persona che mi ha invitato
					exit_sem = false;
					while((!EXIT || sem_reserved) &&  !exit_sem){ 
						if(releaseSem(control.group_set,inv_ind)<0){
							error = errno;
							if(error != EINTR){
								perror("REL_STATE0_GROUP");
								kill(getppid(),SIGTERM);
							}
						}
						else {
							exit_sem = true;
							sem_reserved--;
						}
					}

					//Rilascio mio semaforo
					exit_sem = false;
					while((!EXIT || sem_reserved) &&  !exit_sem){ 
						if(releaseSem(control.group_set,index_group)<0){
							error = errno;
							if(error != EINTR){
								perror("REL_STATE0_GROUP");
								kill(getppid(),SIGTERM);
							}
						}
						else {
							exit_sem = true;
							sem_reserved--;
						}
					}

					//Se sono entrato nel gruppo avviso il proprietario 
					if(weight && !EXIT){ 
						invite.type = my_group.leader*pow(10,(int)log10(1)+1)+1;// type = matr.leader concatenato a 1  
						sprintf(invite.mtext,"%d", myself.matr);
						boolean ex = false;
						while(!ex && !EXIT){ 
							check = msgsnd(control.start_queue,&invite, (sizeof(invite)-sizeof(long)),0);
							if(check < 0){
								error = errno;
								if(error != EINTR){
									perror("MSGSND 0a");
									kill(getppid(),SIGTERM);
								}
							}
							else{
								ex = true;
							}
						}
						//Aggiorno la mia copia locale di Student (myself) e vado nello stato 2
						if(!EXIT){
							enterGroup(inv_mat);
						}
					}	
					//Avviso Studente che mi ha invitato che rifiuto il suo invito						
					else{
						invite.type = inv_mat*pow(10,(int)log10(1)+1)+0;// tipo = matr.leader concatenato a 0  
						sprintf(invite.mtext,"%d", myself.matr);
						boolean ex = false;
						while(!ex && !EXIT){//ALL'ULTIMO ALARM ESCE
							check = msgsnd(control.start_queue,&invite, sizeof(invite)-sizeof(long),0);
							if(check < 0){
								error = errno;
								if(error != EINTR){
									perror("MSGSND 0b");
									kill(getppid(),SIGTERM);
								}
							}
							else{
								ex = true;
							}
						}
					}	
				}
				break;			
			case 1:
				//Invio degli inviti
				num_response_received += checkEntranceMessages() + dequeueRejectionMessages(); //Cerca nella coda i messaggi di risposta (positivi e negativi)
				if(!nof_invites || num_msg_sent == 5){ //Per non stare sempre nella fase di invito dopo un certo numero di inviti provo a cambiare stato
					if(!(num_msg_sent-num_response_received) && student_state == inviting && my_group.num_members == 1){ //Se non hai ancora messaggi in attesa puoi cambiare
						student_state = searching;
						num_msg_sent = 0;
						num_response_received = 0;
						alarm(SIM_TIME/8); //per cambiare stato quando si è nella fase di ttesa di messaggi
						wait_for_swap = false;
					}
					go_invite = false;
				}

				//Se sei il capo di un gruppo con altre persone oltre a te non puoi cambiare quindi continua ad invitare
				if(my_group.num_members >1 && !my_group.closed && !EXIT){
					if(my_group.grade >= 21 && ACTUAL_TIME == (int)SIM_TIME/4){
						//Prenoto semaforo per chiusura del gruppo anticipata
						exit_sem = false;
						while(!EXIT && !exit_sem){
							if(reserveSem(control.group_set,index_group)<0){
								error = errno;
								if(error != EINTR){
									perror("RES_CLOSE_GROUP");
									kill(getppid(),SIGTERM);
								}
							}
							else {
								exit_sem = true;
								sem_reserved++;
							}
						}
						//Se non è finita la simulazione chiudi il gruppo evai nello stato finale
						if(!EXIT){
							my_group = (*(groups+index_group));
							my_group.closed = true;
							myself.group_closed = true;
							(*(groups+index_group)) = my_group;
							student_state = group_closed;
						}

						//Rilascio semaforo
						exit_sem = false;
						while((!EXIT|| sem_reserved)&& !exit_sem){
							if(releaseSem(control.group_set,index_group)<0){
								error = errno;
								if(error != EINTR){
									perror("REL_CLOSE_GROUP");
									kill(getppid(),SIGTERM);
								}
							}
							else {
								exit_sem = true;
								sem_reserved--;
							}
						}
					}
					//Se non ho chiuso il gruppo posso andare a invitare
					else{
						go_invite = true;
					}
				}

				//Se hai ancora inviti e non hai chiuso il gruppo precedentemente: Invita!
				if(nof_invites && student_state == inviting && go_invite && !EXIT){ 
					boolean found = false, ctrl = true;
					student target; // copia locale della persona che vuoi invitare
					int skip = 0; //Variabile per saltare una persona
					while(!found && index < POP_SIZE && !EXIT){ //Vai avanti finchè non trovi una persona da invitare o finisci l'elenco
						//Prenoto semaforo del target. ATTENZIONE: semop fatta con flag NO_WAIT così da poterlo saltare se già occupato
						exit_sem = false;
						while(!EXIT && !exit_sem){
							if(reserveSemNOWAIT(control.group_set,index)<0){
								error = errno;
								if(error != EAGAIN){
									perror("RES_GET_STUDENT");
									kill(getppid(),SIGTERM);
								}
								else{
									skip = 1;
									exit_sem = true;
								}
							}
							else {
								exit_sem = true;
								sem_reserved = 1;
							}
						}
						//Se non ho saltato il target vado a vedere se è un possibile candidato per il mio gruppo
						if(!skip){
							student target = (*(groups+index)).members[0];//Ottengo la struttura del target
							//Controllo che nonlo abbia già invitato
							for(int i = 0; i< POP_SIZE; i++){
								if(target.matr == (*(alreadySent+i))){
									ctrl = false;
									break;
								}
							}
							//Se va bene lo scelgo come persona da invitare
							if(target.matr != myself.matr && target.matr != 0 && !target.group_closed && ((myself.matr%2) == (target.matr%2) ) && ctrl && checkMatch(target.preference)){ 
								found = true;
							}
							else{
								ctrl = true;
							}
						}
						//Rilascio semaforo del target
						exit_sem = false;
						while(sem_reserved && !exit_sem){
							if(releaseSem(control.group_set,index)<0){
								error = errno;
								if(error != EINTR){
									perror("REL_GET_STUDENT");
									kill(getppid(),SIGTERM);
								}
							}
							else {
								exit_sem = true;
								sem_reserved = 0;
							}
						}
						index++;
						skip = 0;
					}

					//ho trovato la persona che non ha ancora ricevuto un mio invito
					if(found && !EXIT){
						invite.type = target.matr; // target del messaggio
						sprintf(invite.mtext,"%d", index_group);

						check = msgsnd(control.start_queue,&invite, sizeof(invite)-sizeof(long),IPC_NOWAIT);
						if(check < 0){ // In caso di errore diverso da EAGAIN (coda piena) faccio terminare simulazione
							error = errno;
							if(error != EAGAIN){
								perror("MSGSND 2");
								kill(getppid(),SIGTERM);
							}
						}
						else{//Salvo target nel array di persone invitate
							num_msg_sent++;
							for(int i = 0; i<POP_SIZE; i++){
								if(*(alreadySent+i) == 0){
									*(alreadySent+i) = target.matr;
									break;
								}
							}
							nof_invites--;
						}
					}
					//Se ho percorso tutto l'elenco di persone da invitare riparti dall'inizio
					if(index == POP_SIZE){
						index = 0;
					}
					autoRefuse(); //Rifiuta automaticamente gli inviti ricevuti
				}
				break;
			case 2://Aspetto la fine della simulazione
				//Pulisce la coda da i messaggi che mi sono arrivati
				autoRefuse();
				dequeueRejectionMessages();
				break;
		}
	}

	//Avviso l'Handler che sono uscito dalla simulazione
	while(releaseSem(control.set_semaphore,5)<0){
		error = errno;
		if(error != EINTR){
			perror("REL_HANDLER_GO");
			kill(getppid(),SIGTERM);
		}
	}

	//Aspetto che l'Handler  mi dica di andare a ricevere i voti
	check =  reserveSem(control.set_semaphore,4);
	while(check<0){
		error = errno;
		if(error != EINTR){
			perror("RES_STUDENT_GO");
			kill(getppid(),SIGTERM);
		}
		else{
			check =  reserveSem(control.set_semaphore,4);
		}
	}

	//Provo a ricevere i voti
	int exit_rec = 1;
	while(exit_rec){
		check = msgrcv(control.start_queue,&invite,sizeof(invite)-sizeof(long),myself.matr,0);
		if(check < 0){
			error = errno;
			if(error != EINTR){
				perror("MSGRCV ");
				kill(getppid(),SIGTERM);
			}
		}
		else {
			exit_rec = 0;
		}
	}

	//Salvo il mio voto nella copia locale
	myself.SO = atoi(invite.mtext);

	//Blocco l'accesso al file di log per scivere i miei dati
	int e = 1;
	while(e){
		if(reserveSem(control.set_semaphore,3)<0){
			error = errno;
			if(error != EINTR){
				perror("RESERVE FINALE ");
				kill(getppid(),SIGTERM);
			}
		}
		else e = 0;
	}

	//Scrivo i miei dati
	FILE* data_file = fopen("StudentData", "a");
	fprintf(data_file, "SONO LO STUDENTE %d, QUESTI SONO I MIEI RISULTATI:\nMIO VOTO DI ARCHITETTURE: %d\nMIO VOTO DI SISTEMI OPERATIVI: %d\nMIO LEADER: %d\n\n",myself.matr, myself.arch_grade, myself.SO, my_group.leader);
	fclose(data_file);

	//rilascio il semaforo
	if(releaseSem(control.set_semaphore,3)<0){
		perror("RELEASE FINALE ");
		kill(getppid(),SIGTERM);
	}
	
	//Mi stacco dalle memorie, libero la memoria allocata e termino
	shmdt(data);
	shmdt(groups);
	shmdt(phase);
	free (alreadySent);
	exit(EXIT_SUCCESS);
	
}

//Funzione per decidere se accettare un invito.
//Ritorna true o false
boolean checkAccept(int inv_grade, int inv_pref){
	boolean ris = false;
	if(ACTUAL_TIME == SIM_TIME ){ // se ho ancora tutto il tempo accetto chi ha il voto più alto del mio
		if(inv_grade >= myself.arch_grade){
			ris = true;
		}
	}
	else if(ACTUAL_TIME == (int)SIM_TIME/2){//Se è passato metà del tempo accetto chi ha la mia stessa preferenza o chi ha un voto più alto di 20
		if(inv_pref == myself.preference || max(inv_grade,myself.arch_grade) > 20){
			ris =true;
		}
	}
	else{//Se siamo a 1/4 o 3/4 del tempo accetto tutti
		ris = true;
	}
	return ris;
}

//Funzione per decidere se invitare uno Studente.
//Ritorna true o false
boolean checkMatch(int inv_pref){
	boolean ris = false;
	if(ACTUAL_TIME == SIM_TIME || ACTUAL_TIME == 3*(int)(SIM_TIME/4)){//Fino a metà del tempo invito chi ha la mia stessa preferenza
		if(inv_pref == myself.preference){
			ris = true;
		}
		else ris = false;	
	}
	else {//Da metà del tempo fino alla fine invito tutti
		ris = true;
	}
	return ris;
}

//Libera memorie condivise e free della memoria allocata e termina.
//Chiamato in caso di errore
void handle_shut_down(int sig){
	if(sig == SIGTERM){
		printf("Spegnimento del processo\n");
		free (alreadySent);
		shmdt(groups);
		shmdt(data);
		shmdt(phase);
		exit(-1);
	}
}

//Handler per gestire il segnale di tipo SIGUSR1
void handle_end(int sig){
	EXIT = true;
}

//Handler per gestire il segnale di allarme
void handle_change_state(int sig){
	if(sig == SIGALRM){
		if(student_state == searching){
			wait_for_swap = true;
		}
	}
}

//Pulizia dalla coda dei messaggi di rifiuto
//Ritorna il numero dei messaggi di rifiuto ricevuti
int dequeueRejectionMessages(){
	struct msgg response;
	int error = 0, ret = 0, r_mat; // ret: numero di messaggi ricevuti
	char* r_string;

	//controlla se mi sono arrivati dei rifiuti
	int check = -1;
	while(error != ENOMSG && !EXIT){
		check = msgrcv(control.start_queue,&response,sizeof(response)- sizeof(long), (myself.matr * pow(10, (int)log10(1)+1))+0, IPC_NOWAIT);
		if(check>0){//Se trovo un messaggio per me
			r_string = response.mtext;
			r_mat = atoi(r_string);
			ret ++; //Incremento numero dei messaggi letti
			//Tolgo la persona dall array di persone invitate
			for(int i = 0; i< POP_SIZE; i++){
				if(*(alreadySent+i) == r_mat){
					*(alreadySent+i) = 0;
					break;
				}
			}
		}
		else{//Gestione caso di errore
			error = errno;
			if(error != ENOMSG){
				perror("ERROR RECEIVING DEQUEUING THE MESSAGE");
				kill(getppid(),SIGTERM);
			}
		}
	}
	return ret;
}

//Rifiuta automaticamente un invito
void autoRefuse(){
	struct msgg response;
	int error = 0;

	//controlla se mi sono arrivati dei rifiuti
	int check = -1;
	while(error != ENOMSG && !EXIT){
		check = msgrcv(control.start_queue,&response,sizeof(response), myself.matr, IPC_NOWAIT);
		if(check<0){//Gestione caso di error
			error = errno;
			if(error != ENOMSG){
				perror("ERROR RECEIVING DEQUEUING THE MESSAGE");
				kill(getppid(),SIGTERM);
			}
		}
		else{//Rispondo negativamente all'invito ricevuto
			char* inv_string = response.mtext;
			char* char_inv = strtok(inv_string, " ");
			int inv_mat;
			int inv_ind = atoi(char_inv);
			inv_mat = (*(groups+inv_ind)).leader;
			response.type = inv_mat*pow(10,(int)log10(1)+1)+0;// tipo = matr.leader concatenato a 0 
			sprintf(response.mtext,"%d", myself.matr);
			boolean ex = true;
			while(ex && !EXIT){// Invioil messaggio. Gestisce il caso in cui msgsnd viene bloccata da un segnale
				check = msgsnd(control.start_queue,&response, sizeof(response)-sizeof(long),0);
				if(check < 0){
					error = errno;
					if(error != EINTR){
						perror("MSGSND 0c ");
						kill(getppid(),SIGTERM);
					}
					
				}
				else{
					ex = false;
				}
			}
		}
	}
}

//Legge i messaggi di entrata nel gruppo, aggiorna la copia locale del gruppo my_group e chiude il gruppo se pieno
//Ritorna numero dei messaggi letti
int checkEntranceMessages(){
	struct msgg response;
	int error, ret = 0, check = 0, i = 0, res_sem = 0;;
	boolean found = false;
	student new_member;
	long my_type = myself.matr*pow(10,(int)log10(1)+1)+1; //Messaggi di accettazione: tipo = mia matr concat. 1
	while(error != ENOMSG && !EXIT){ // Finchè i messaggi per me non sono finiti continua a leggere nella coda
		check = msgrcv(control.start_queue, &response, (sizeof(response)-sizeof(long)), my_type, IPC_NOWAIT); 
		if(check < 0){//Gestione caso di errore
			error = errno;
			if(error != ENOMSG ){ 
				perror("MSGRCV ENTRANCE");
				kill(getppid(),SIGTERM);
			}
		}
		else{
			ret ++;//incrementa numero di messaggi letti
			char* matr_char = response.mtext;
			int req_matr = atoi(matr_char); // matricola dello studente che è entrato nel gruppo

			//Rimuovo lo studente dall array degli studenti invitati
			for(int i = 0; i< POP_SIZE; i++){
				if(*(alreadySent+i) == req_matr){
					*(alreadySent+i) = 0;
					break; // da cambiare!!
				}
			}
			//Se questa è la prima persona che entr divento leader
			if(my_group.num_members == 1){
				myself.leader = myself.matr;
			}

			//Prenoto semaforo dei gruppi per aggiornare il mio gruppo
			exit_sem = false;
			while(!EXIT && !exit_sem){
				if(reserveSem(control.group_set,index_group)<0){
					error = errno;
					if(error != EINTR){
						perror("RES_ENTRANCE");
						kill(getppid(),SIGTERM);
					}
				}
				else {
					exit_sem = true;
					res_sem++;
				}
			}
			
			//Se il gruppo è pieno, lo chiudo
			if((*(groups+index_group)).num_members == myself.preference && !EXIT){
				(*(groups+index_group)).closed = true;
				myself.group_closed = true;
				student_state = group_closed;
			}
			//Aggiorno il gruppo
			my_group = *(groups+index_group);
			exit_sem = false;
			
			//Rilascio il semaforo
			while((!EXIT||res_sem) && !exit_sem){
				if(releaseSem(control.group_set,index_group)<0){
					error = errno;
					if(error != EINTR){
						perror("REL_ENTRANCE");
						kill(getppid(),SIGTERM);
					}
				}
				else {
					exit_sem = true;
					res_sem--;
				}
			}
		}
	}
	return ret;
}

//Aggiorno la mia copia locale di studente e cambio stato
void enterGroup(int leader_matr){
	myself.leader = leader_matr;
	myself.group_closed = true;
	student_state = group_closed;
}
