#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <sys/sem.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>


struct msgg {
	long type;
	char mtext[60];
};

typedef enum {false,true} boolean;

typedef struct {
	int matr;
	int arch_grade;
	int SO;
	int preference;
	int leader;
	boolean group_closed;
}student;

typedef struct {
	int leader;
	student members[4];
	int preference;
	int num_members;
	int grade;
	boolean closed;
}group;

typedef struct {
	pid_t *processes;
	int start_queue;
	int set_semaphore;
	int group_set;
	int mem_groups;
	int mem_data;
	int mem_time;
}control_data;

	
union semunn {
	int val;
	struct semid_ds* buf;
	unsigned short* array;
	struct seminfo* __buf;
}arg;

//int randomN(int lower, int upper);
int max(int n1, int n2);

int opSem(int semId, int semNum,int num);
int reserveSem(int semId, int semNum);
int reserveSemNOWAIT(int semId, int semNum);
int releaseSem(int semId, int semNum);

