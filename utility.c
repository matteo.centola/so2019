#include "utility.h"

int reserveSem(int semId, int semNum) {
	struct sembuf sops;
	sops.sem_num = semNum;
	sops.sem_op = -1;
	sops.sem_flg = 0;
	return semop(semId, &sops, 1);
}

int reserveSemNOWAIT(int semId, int semNum){
	struct sembuf sops;
	sops.sem_num = semNum;
	sops.sem_op = -1;
	sops.sem_flg = IPC_NOWAIT;
	return semop(semId, &sops, 1);
}

int releaseSem(int semId, int semNum) {
	struct sembuf sops;
	sops.sem_num = semNum;
	sops.sem_op = 1;
	sops.sem_flg = 0;
	return semop(semId, &sops, 1);
}

int opSem(int semId, int semNum,int num){
	struct sembuf sops;
	sops.sem_num = semNum;
	sops.sem_op = num;
	sops.sem_flg = 0;
	return semop(semId, &sops, 1);
}


int max(int n1, int n2){
	if(n1>n2){
		return n1;
	}
	else return n2;
}
