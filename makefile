all: Handler   Student
Handler: Handler.o utility.o
	gcc -o Handler Handler.o utility.o
Student: Student.o utility.o
	gcc -o Student Student.o utility.o -lm
Handler.o: Handler.c
	gcc -c Handler.c
Student.o: Student.c
	gcc -c Student.c 
utility.o: utility.c
	gcc -c utility.c
clean:
	rm -f *.o

