
#include <errno.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <math.h>
#include <time.h>
#include "utility.h"
#define randomN(min, max) (rand() %(max-min+1))+min
#define randomize srand((unsigned)time(NULL))
  
int SIM_TIME;
int POP_SIZE;
int countdown;
int* actual_time;

static int child_terminated;
student stud;
control_data control; //Struttura per la gestione dei dati
int incrementSem(int semId, int semNum,int num);
int getGroupDistribution(int percentage, int size);
void handle_error(int sig);
void handle_alarm(int sig);
void handle_wakeup(int sig);
void terminate_and_end();

int main(){
	int i, x,  index = 0;
	int nof_invites, max_rejects;  
	int group_number, two_group, three_group, four_group, two_group_perc, three_group_perc, four_group_perc, random, exit_while;	
	int arch_average, so_average;
	int grade_affluence[13];
	int zero_affluence = 0;
	arch_average = 0;
	so_average = 0;
	exit_while = 0;
	randomize; // Set seed per random
	pid_t procpid;
	char arg_nof_elem[30];
	char* configPath = "config"; //path del file config
	char buffer1[10], buffer2[10];
	int index_shared_memory = 0;
	
	//Impostazione array per voti
	for(int i = 0; i < 13; i++){
		grade_affluence[i] = 0;
	}

	//Apertura file di config
	FILE* conf = fopen(configPath,"r");
	if(conf == NULL){
		printf("fopen error");
		exit(EXIT_FAILURE);
	}

	//Caricamento POP_SIZE
	fscanf(conf,"%s %d",buffer1, &POP_SIZE);
	if(POP_SIZE<=0){
		printf("STUDENTI NON PRESENTI! CHIUDO LA SIMULAZIONE\n");
		exit(EXIT_FAILURE);
	}
	
	child_terminated = POP_SIZE; //Intero utilizzato per la chiusura
	control.processes = malloc(sizeof(pid_t)*POP_SIZE); //Allocazione della memoria per un array di pid di Studenti

	//Caricamento delle percentuali
	for(i = 0; i < 3; i++){
		fscanf(conf,"%s %s",buffer1,buffer2); 
		group_number = atoi(buffer1);
		switch(group_number){
			case 2:
			two_group_perc = atoi(buffer2);
			break;
			
			case 3:
			three_group_perc = atoi(buffer2);
			break;

			case 4:
			four_group_perc = atoi(buffer2);
			break;
		}
	}
	//Controllo che le percentuali siano giuste
	if(two_group_perc+three_group_perc+four_group_perc != 100){
		printf("Le percentuali non rispecchiano la totalità degli studenti\n");
		exit(EXIT_FAILURE);
	}
	two_group = getGroupDistribution(two_group_perc, POP_SIZE);
	three_group = getGroupDistribution(three_group_perc, POP_SIZE);
	four_group = getGroupDistribution(four_group_perc, POP_SIZE);
	
	fscanf(conf,"%s %d",buffer1, &SIM_TIME); //Caricamento SIM_TIME

	fscanf(conf,"%s %d",buffer1, &nof_invites); //Caricamento nof_invites

	fscanf(conf,"%s %d",buffer1, &max_rejects); // Caricamento max_rejects

	//Chiusura file di config
	if(fclose(conf)<0){
		perror("FCLOSE: ");
		exit(EXIT_FAILURE);
	}

	//Creazione e impostazione di un Handler per gestire segnali di terminazione
	if(signal(SIGTERM,handle_error)<0){
		printf("Error sigaction error");
		exit(-1);
	}

	//Creazione e impostazione di un Handler per gestire segnali di allarme
	if(signal(SIGALRM, handle_alarm)<0){
		printf("error signal");
		exit(-1);
	}

	//Inizializzazione strutture IPC 
	control.start_queue = msgget(getpid(),IPC_CREAT|0666); 
	if(control.start_queue < 0){
		perror("MSGGET");
		exit(-1);
	}

	//Memoria di Gruppi
	control.mem_groups = shmget(IPC_PRIVATE, (sizeof(group)*POP_SIZE),IPC_CREAT|0666);
	if(control.mem_groups < 0){
		perror("SHMGET");
		exit(-1);
	}
	group *group_pointer = shmat(control.mem_groups,NULL,0);

	//Memoria di Studenti
	control.mem_data = shmget(IPC_PRIVATE, (sizeof(student)*POP_SIZE),IPC_CREAT|0666);
	if(control.mem_data < 0){
		perror("SHMGET");
		exit(-1);
	}

	//Semaforo per l'aggiornamento sul tempo rimanente agli Studenti
	control.mem_time = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT|0666);
	if(control.mem_time < 0){
		perror("SHMGET");
		exit(EXIT_FAILURE);
	}
	actual_time = shmat(control.mem_time, NULL, 0);
	(*actual_time) = 0;

	// semaforo per la modifica della memoria condivisa con gli studenti
	control.set_semaphore = semget(IPC_PRIVATE,6,IPC_CREAT|0666);  
	if(control.set_semaphore < 0){
		perror("SEMGET ");
		exit(-1);
	}
	semctl(control.set_semaphore,0,SETVAL,0); // per partenza
	semctl(control.set_semaphore,1,SETVAL,0); // per partenza
	semctl(control.set_semaphore,2,SETVAL,1); // per ricevere i dati dello Studente
	semctl(control.set_semaphore,3,SETVAL,1); // per inizializzare i dati dei Gruppi
	semctl(control.set_semaphore,4,SETVAL,0); // per fine
	semctl(control.set_semaphore,5,SETVAL,0); // per fine 

	//creazione di un set di semafori per gli Studenti, uno per Studente, tutti inizializzati a 1
	unsigned short semvalues[POP_SIZE];
	control.group_set = semget(IPC_PRIVATE,POP_SIZE,IPC_CREAT|0666); 
	if(control.group_set < 0){
		perror("SEMGET ");
		exit(-1);
	}
	for(int i = 0; i<POP_SIZE;i++){
		semvalues[i] = 1;
	}
	arg.array = semvalues;
	int con = semctl(control.group_set, 0, SETALL, arg);
	if(con<0){
		perror("SEMCTL2 ");
		exit(EXIT_FAILURE);
	}
	int val = semctl(control.group_set,POP_SIZE-1, GETVAL, arg);

	// Salvataggio dei dati in argv per passarli ai processi Studente
	char *argVec[10];
	argVec[0] = malloc(sizeof(char)*10);
	sprintf(argVec[0],"%s","./Student");
	argVec[1] = malloc(sizeof(int));
	sprintf(argVec[1],"%d",control.set_semaphore);
	argVec[2] = malloc(sizeof(int));
	argVec[3] = malloc(sizeof(int));
	sprintf(argVec[3],"%d",control.mem_groups);
	argVec[4] = malloc(sizeof(int));
	sprintf(argVec[4],"%d",nof_invites);
	argVec[5] = malloc(sizeof(int));
	sprintf(argVec[5],"%d",control.mem_data);
	argVec[6] = malloc(sizeof(int));
	sprintf(argVec[6],"%d",max_rejects);
	argVec[7] = malloc(sizeof(int));
	sprintf(argVec[7],"%d",SIM_TIME);
	argVec[8] = malloc(sizeof(int));
	sprintf(argVec[8],"%d",POP_SIZE);
	argVec[9] = malloc(sizeof(int));
	sprintf(argVec[9], "%d", control.mem_time);
	argVec[10] = malloc(sizeof(int));
	sprintf(argVec[10],"%d",control.group_set);
	argVec[11] = NULL;

	//Creazione e avvio dei processi Studente
	for(i = 0; i < POP_SIZE; i++){
		random = randomN(2,4);
		//Assegnazione di una preferenza allo Studente	
		while(exit_while==0){
			if(random == 2 && two_group>0){
				two_group-=1;
				exit_while = 1;
			}
			else if(random == 3 && three_group>0){
				three_group-=1;
				exit_while = 1;
			}
			else if(random == 4 && four_group>0){
				four_group-=1;
				exit_while = 1;
			}
			else{
				random = randomN(2,4);
			}
		}
		exit_while = 0;
		switch(procpid = fork()){ 
		case -1:
			//Errore nell'esecuzione della fork
			perror("FORK ");
			exit(EXIT_FAILURE);
			break;
		case 0:
			//Processo studente crea struttura Studente e la salva nella memoria condivisa per passarla al file eseguibile
			stud.matr = getpid();
			stud.arch_grade = randomN(18,30);
			stud.preference = random; // rinominarlo che si capisce male
			stud.leader = 0;
			stud.group_closed = false;
			
			if(reserveSem(control.set_semaphore,3)<0){
				perror("INITIALIZING STUDENT: RES");
				handle_error(SIGTERM);
			}
			student* stud_vec;
			stud_vec = (student*)shmat(control.mem_data,NULL,0);
			*(stud_vec+index_shared_memory) = stud;
			shmdt(stud_vec);
			if(releaseSem(control.set_semaphore,3)<0){
				perror("INITIALIZING STUDENT: REL");
				handle_error(SIGTERM);
			}
			sprintf(argVec[2],"%d",random); // rinomina random
			execve("./Student", argVec, NULL);
			perror("EXECVE ");
			exit(-1);
			break;		
		default:
			//Salvataggio dei pid dei processi in un array per poterli chiudere in caso di errore
			control.processes[index_shared_memory] = procpid;
			index_shared_memory++;
			break;
		}
	}

	//Attesa degli studenti
	if(opSem(control.set_semaphore,1,-(POP_SIZE))<0){
		perror("OP_END1");
		handle_error(SIGTERM);
	}

	//controllo per vedere se è stato inserito un SIM_TIME valido
	if(SIM_TIME == 0){
		printf("HAI MESSO TEMPO = 0! TERMINO\n");
		terminate_and_end();
	}
	
	printf("inizio simulazione\n");
	
	//Faccio iniziare la simulazione ai processi figli
	if(opSem(control.set_semaphore,0,POP_SIZE)<0){
		perror("OP_START1");
		handle_error(SIGTERM);
	}
	
	//controllo per vedere se SIM_TIME sia divisibile per 4 0 minore di 4
	if(SIM_TIME%4){
		countdown = 4;
	}
	else{
		countdown = 3;
	}
	if(SIM_TIME<4){
		countdown = 0;
	}
	
	//Inizio simulazione
	if(SIM_TIME>=4){
		alarm(SIM_TIME/4);
		pause();
		alarm(SIM_TIME/4);
		pause();
		alarm(SIM_TIME/4);
		pause();
		alarm(SIM_TIME/4);
		pause();
	}
	//in caso di SIM_TIME non divisibile per 4 riprendo i secondi persi
	if(SIM_TIME%4||SIM_TIME<4){
		alarm(SIM_TIME%4);
		pause();
	}

	//Attesa che tutti i processi siano usciti dalla simulazione
	if(opSem(control.set_semaphore,5,-POP_SIZE)<0){ 
		perror("OP WAITING");
		handle_error(SIGTERM);
	}
	
	//Pulizia della coda
	struct msgg rm;
	int empty = 1;
	rm.type = 0;
	while(empty){ //svuoto coda dei messaggi
		if(msgrcv(control.start_queue,&rm,sizeof(rm),0,IPC_NOWAIT)<0){
			if(errno == ENOMSG){
				empty = 0;
			}
		}
	}

	//Creazione log per gli studenti
	FILE* f = fopen("StudentData","w");
	fprintf(f,"QUESTI SONO I DATI DEGLI STUDENTI:\n");
	fclose(f);

	//Invio dei voti agli studenti
	if(reserveSem(control.set_semaphore,3)<0){
		perror("ENDING: RES");
		handle_error(SIGTERM);
	}

	if(opSem(control.set_semaphore,4,POP_SIZE)<0){
		perror("OP END");
		handle_error(SIGTERM);
	}
	for(int i = 0; i < POP_SIZE;i++){ // invio ad ogni studente il suo voto
		struct msgg grade_message;
		int ind = 0;
		group t_group;
		t_group =(*(group_pointer+i));
		
		if(t_group.leader != 0){
			while(ind < t_group.num_members){
				grade_message.type = t_group.members[ind].matr;
				arch_average += t_group.members[ind].arch_grade;
				if(t_group.members[ind].preference == t_group.num_members && t_group.closed){
					sprintf(grade_message.mtext,"%d",t_group.grade);
					grade_affluence[t_group.grade%18]++;
					so_average += t_group.grade;
				}
				else if(t_group.closed && t_group.members[ind].preference != t_group.num_members && t_group.grade > 21){
					sprintf(grade_message.mtext,"%d",(t_group.grade-3));
					grade_affluence[(t_group.grade-3)%18]++;
					so_average += (t_group.grade-3);
				}
				else{
					sprintf(grade_message.mtext,"%d",0);
					zero_affluence++;
				}
				int ch = msgsnd(control.start_queue,&grade_message,(sizeof(grade_message)-sizeof(long)),0);
				if(ch <0){
					perror("MSGSND ");
					terminate_and_end;
				}
				ind++;
			}
		}	
	}
	if(releaseSem(control.set_semaphore,3)<0){
		perror("ENDING: REL");
		handle_error(SIGTERM);
	}

	//Attesa dei figli per non creare processi zombie
	printf("FINITO DI INVIARE I VOTI\n");
	while(child_terminated > 0){ //wait dei processi figli 
		int status = 0;
		wait(&status);
		child_terminated--;
	}

	//stampa delle affluenze
	char aff_path[20] = "Affluence";
	f = fopen(aff_path,"w");
	fprintf(f,"BOCCIATI %d\n", zero_affluence);
	fclose(f);

	f = fopen(aff_path,"a");
	for(int i = 0; i < 13; i++){
		fprintf(f,"%d %d\n",i+18,grade_affluence[i]);
	}
	fclose(f);

	//stampa delle medie 
	char arch_av_path[10] = "Averages";
	arch_average = arch_average/POP_SIZE;
	so_average = so_average/POP_SIZE;
	f = fopen(arch_av_path,"w");
	fprintf(f,"QUESTE SONO LE MEDIE DEI VOTI:\n");
	fclose(f);
	f = fopen(arch_av_path,"a");
	fprintf(f,"MEDIA DEI VOTI DI ARCHITETTURE DEGLI ELABORATORI:   %d\n",arch_average);
	fprintf(f,"\nMEDIA DEI VOTI DI SISTEMI OPERATIVI:   %d\n",so_average);
	fclose(f);

	//chiusura strutture IPC e free delle zone di memoria allocate 
	msgctl(control.start_queue,IPC_STAT, NULL);
	if(msgctl(control.start_queue, IPC_RMID, NULL)<0){ 
		perror("MSGCTL");
		exit(-1);
	}
	
	if(semctl(control.set_semaphore,0,IPC_RMID,NULL)<0){ 
		perror("SEMCTL");
		exit(EXIT_FAILURE);
	}
	
	if(semctl(control.group_set,0,IPC_RMID,NULL)<0){
		perror("SEMCTL");
		exit(EXIT_FAILURE);
	}

	if(shmdt(group_pointer)<0){
		perror("SHMDT");
		exit(EXIT_FAILURE);
	}

	if(shmdt(actual_time)<0){
		perror("SHMDT");
		exit(EXIT_FAILURE);
	}

	if(shmctl(control.mem_time,IPC_RMID,NULL)<0){
		perror("SHMCTL");
		exit(EXIT_FAILURE);	
	}

	if(shmctl(control.mem_groups,IPC_RMID,NULL)<0){
		perror("SHMCTL");
		exit(EXIT_FAILURE);	
	}

	if(shmctl(control.mem_data,IPC_RMID,NULL)<0){
		perror("SHMCTL");
		exit(EXIT_FAILURE);	
	}

	free(control.processes);
	exit(EXIT_SUCCESS);
	
}

//Gestore del segnale di terminazione
void handle_error(int sig){
	if(sig == SIGTERM){
		printf("ERRORE INASPETTATO, TERMINO L'ESECUZIONE DEL PROGRAMMA\n");
		terminate_and_end();
	}
}

//Gestore del segnale di allarme
void handle_alarm(int sig){
	if(countdown){
		countdown--;
		(*actual_time)++; 
	}
	else{
		for(int i = 0; i < POP_SIZE; i++){ 
			kill(control.processes[i], SIGUSR1);
		}
	}
	
}

//Terminazione dei processi Studente e chiusura di strutture IPC
void terminate_and_end(){
	for(int i = 0; i < POP_SIZE; i++){ //termino figli
		kill(control.processes[i], SIGTERM);
		printf("TERMINO I PROGRAMMI\n");
		int status = 0;
		wait(&status);
	}
	msgctl(control.start_queue, IPC_RMID, NULL);

	semctl(control.set_semaphore,0,IPC_RMID,NULL);

	if(shmctl(control.mem_time,IPC_RMID,NULL)<0){
		perror("SHMCTL");
		exit(EXIT_FAILURE);	
	}

	if(semctl(control.group_set,0,IPC_RMID,NULL)<0){
		perror("SEMCTL");
		exit(EXIT_FAILURE);
	}

	shmctl(control.mem_groups,IPC_RMID,0);

	shmctl(control.mem_data,IPC_RMID,0);
	free(control.processes);
	
	exit(-1);
}

//Trasformazione una percentuale in un numero arrotondando per difetto o eccesso in caso di numero con la virgola
int getGroupDistribution(int percentage, int size){
	float number_in_float,decimal_part;
	int final_value;
	number_in_float = (float)size * percentage / 100;
	final_value = (int)number_in_float;
	decimal_part = number_in_float-final_value;
	if(decimal_part > 0.4){
		final_value+=1;
	}
	return final_value;
}



